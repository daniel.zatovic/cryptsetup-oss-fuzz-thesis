\chapter{Conclusion}

The thesis started by examining the theoretical background of software testing and the evolution of fuzz testing. We introduced fuzzing as an imprecise yet reliable way to discover bugs in complex software. We described various fuzzing techniques, some of which we later applied to a real-world target.

Fuzzing is, in its nature, a brute-force approach to bug discovery. One of the keys to success in fuzzing is to provide enough computational power to the fuzzer. Therefore, we research the possibility of running multiple fuzzer instances, which share a single corpus, on a distributed system.

We described the engineering decisions made in the OSS-Fuzz project to build a distributed fuzzing ecosystem on top of conventional fuzzers. We document how the OSS-Fuzz project manages the bug lifecycle. The lifecycle stages include bug discovery, reporting to developers, verifying the fix, identifying the fixing commit and automatic regression testing.

We have decided to run the fuzzer on `cryptsetup`, a widely used Linux full disk encryption utility. We have targeted this program because it is designed to secure user data, so it is vital to ensure the security of the tool itself. We have analysed the utility and identified all possible attack vectors. We identified the metadata stored on the disk as the most significant one. Therefore, we have examined the Linux Unified Key Setup version 2 on-disk file format in detail. The LUKS2 format also contains a structured JSON area, apart from the binary header.

We propose a nonintelligent fuzz `cryptsetup` target in the practical part. We have integrated the fuzz target into the `cryptsetup` build system and proposed how to build it in the context of OSS-Fuzz infrastructure. We have described the process of implementing a more advanced structure-aware fuzz target using a custom mutator. We have implemented a custom mutator for the LUKS2 format using Protocol Buffers and `libprotobuf-mutator`. We discuss the performance and differences of both fuzz targets. Finally, we have addressed and reported the discovered vulnerabilities.

# Future work

We have shown that the selected approach is promising and can lead to discovering potential security issues. However, there is still room for improvement. The proposed solution only fuzzes the LUKS2 format. We could use a similar approach for fuzzing other formats supported by `cryptsetup`, including BitLocker and VeraCrypt. Since alternative implementations, different from `cryptsetup`, exist for these two formats, differential fuzzing could be used to uncover discrepancies between the implementations.

Additional work needs to be done to upstream the changes. The most crucial issue is determining how to disable checksum calculation in fuzzing builds to speed up fuzzing. A compromise needs to be found on how to disable checksum without risking misconfiguration issues leading to endangering security or data integrity. A pull request must also be sent and accepted to the OSS-Fuzz project.

As we have shown, the structure-aware fuzzer is too rigid in some cases, leading to missing some lower-level bugs. A generic JSON mutator could be proposed as a next step that allows the generation and mutation of arbitrary JSON objects. We can draw inspiration from the `jsoncpp` project custom mutator that is present in the OSS-Fuzz project \cite{oss-fuzz-github}.

In the future, we could add custom error detection. It could be implemented using a fake `dm-crypt` backend to detect the activation of overlapping segments of devices. We could add more `asserts` in the fuzz build by manually inspecting the source code and locating places of interest.

Device activation requires interaction with the kernel using root privileges unavailable in the OSS-Fuzz environment. Moreover, the activation would be slow. Therefore, the current fuzz targets only call the `crypt_load` function, which performs just metadata validation. However, if the fake `dm-crypt` implementation was available, we could simulate device activation.

Similarly, adding or removing keyslots invokes KDF, which is designed to be slow. We could add a similar mock implementation of cryptographic functions. We could then run the code responsible for adding or removing keyslots, unveiling the true potential of the custom mutator.
