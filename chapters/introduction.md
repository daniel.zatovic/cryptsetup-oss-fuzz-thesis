\chapter{Introduction}

With the high and growing availability of computers, they are becoming an increasingly profound part of our lives. The more they become a part of our existence, the more will they need to store sensitive information about ourselves. The need for privacy is present in different spheres, including personal, activist and commercial. As we transfer parts of our lives into the digital domain, we need to introduce new security mechanisms.

Developers proposed various disk encryption tools to ensure the confidentiality of data at rest. Like every piece of software, disk encryption utilities are also prone to mistakes. The severity of the bugs in disk encryption programs is even more accented because they are supposed to protect the user. The developers should thoroughly test such software to eliminate as many vulnerabilities as possible.

Fuzz testing randomly generates semi-valid inputs and monitors the program under test for abnormal behaviour. This technique was proven to be very efficient at discovering new bugs with a relatively low amount of effort from the developer.

The Google company has recognized the value of fuzz testing and has developed an automated infrastructure for distributed fuzzing called OSS-Fuzz \cite{oss-fuzz}. Access to OSS-Fuzz is provided for free to all open-source projects. OSS-Fuzz became very successful with over 39000 discovered bugs \cite{oss-fuzz-issues}, and 580 integrated open-source projects \cite{oss-fuzz-github}.

As a program under test, we have chosen the `cryptsetup` \cite{cryptsetup} disk encryption utility, which is used on many Linux distributions. We propose a way to integrate the `cryptsetup` into the OSS-Fuzz project. To enhance the fuzzing process, we introduce a format-specific custom mutator. The selected approach discovered several potential security flaws. All of the bugs were reported and fixed in the upstream repository.

The thesis starts by introducing the concept of software testing and fuzz testing. It describes the fuzz testing techniques and categories. Then it proceeds by describing how to fuzz on a scale, the concept of distributed fuzzing along with the architecture of the OSS-Fuzz project, which is based on the ClusterFuzz \cite{clusterfuzz} project. Description of the `cryptsetup` utility and the Linux Unified Key Setup \cite{luks1,luks2} format follows.

In the chapter on the implementation part, we describe the threat model for `cryptsetup` and propose fuzz targets. We specify how to build the fuzz targets within the OSS-Fuzz infrastructure and integrate them with the `cryptsetup` build system. We document the interface for developing custom mutators using `libfuzzer` \cite{libfuzzer} and `libprotobuf-mutator` \cite{libprotobuf-mutator}. Then we describe the choices that had to be made during the custom mutator development. Finally, we evaluate and discuss the flaws discovered in `cryptsetup` using fuzz testing.
